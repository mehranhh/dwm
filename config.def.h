/* See LICENSE file for copyright and license details. */
/* patches */
#define PATCHATTACHBOTTOM 1
#define PATCHALWAYSCENTER 1
#define BARPERTAG 0

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static const int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft = 0;   	/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int showbar            = 1;     /* 0 means no bar */
static const int topbar             = 1;     /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

static const char *const autostart[] = {
	"st", NULL,
	NULL /* terminate */
};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* identifier     class       instance    title       tags mask     isfloating   monitor    float x,y,w,h         floatborderpx cmd*/
	{ "gimp",         "Gimp",     NULL,       NULL,       0,            1,           -1,        50,50,500,500,        5,             {"gimp", NULL}},
	{ "firefox",      "Firefox",  NULL,       NULL,       1 << 8,       0,           -1,        50,50,500,500,        5,             {"firefox", NULL}},
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

static const TagData tags[] = { 
    // symbol,  layout,            mfact,   nmasters, showbars
    { "1",      &layouts[0],       mfact,   nmaster,  showbar },
    { "2",      &layouts[2],       mfact,   nmaster,  showbar },
    { "3",      &layouts[0],       mfact,   nmaster,  showbar },
    { "4",      &layouts[2],       mfact,   nmaster,  showbar },
    { "5",      &layouts[0],       mfact,   nmaster,  showbar },
    { "6",      &layouts[0],       mfact,   nmaster,  showbar },
    { "7",      &layouts[0],       mfact,   nmaster,  showbar },
    { "8",      &layouts[0],       mfact,   nmaster,  showbar },
    { "9",      &layouts[0],       mfact,   nmaster,  showbar },
};

static ToggleProc toggleprocs[] = {
	/* id               autosatrt   signal       cmd                                                     0 */
	{ "gammastep",      1,          SIGTERM,     "Blue Light Filter",   "eye-solid",         {"gammastep", "-l", "36.51212:51.1251775", "-r", NULL}, 0},
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ KeyPress,   MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "st", NULL };

/* this must be your default keyboard config */
static const char *cmdgamemode0keyboard[] = {"setxkbmap", "-layout", "us,ir,us", "-model", "pc104", "-variant", "colemak,,", "-option", "caps:backspace,grp:alt_shift_toggle", NULL };
static const char *cmdgamemode1keyboard[] = {"setxkbmap", "-layout", "us", "-model", "pc104", "-variant", "colemak", NULL };

#include "movestack.c"
#include "keys.h"
static Key keys[] = {
	/* type       modifier                      key        function        argument */
	{ KeyPress,   MODKEY,                       Key_q,     spawngamemodeoff,{.v = dmenucmd } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_Return,spawn,          {.v = termcmd } },
	{ KeyPress,   MODKEY,                       Key_b,     togglebar,      {0} },
	{ KeyPress,   MODKEY,                       Key_j,     focusstack,     {.i = +1 } },
	{ KeyPress,   MODKEY,                       Key_k,     focusstack,     {.i = -1 } },
	{ KeyPress,   MODKEY,                       Key_i,     incnmaster,     {.i = +1 } },
	{ KeyPress,   MODKEY,                       Key_d,     incnmaster,     {.i = -1 } },
	{ KeyPress,   MODKEY,                       Key_h,     setmfact,       {.f = -0.05} },
	{ KeyPress,   MODKEY,                       Key_l,     setmfact,       {.f = +0.05} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_j,     movestack,      {.i = +1 } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_k,     movestack,      {.i = -1 } },
	{ KeyPress,   MODKEY|Mod4Mask,              Key_h,     incrgaps,       {.i = +1 } },
	{ KeyPress,   MODKEY|Mod4Mask,              Key_l,     incrgaps,       {.i = -1 } },
	{ KeyPress,   MODKEY|Mod4Mask|ShiftMask,    Key_h,     incrogaps,      {.i = +1 } },
	{ KeyPress,   MODKEY|Mod4Mask|ShiftMask,    Key_l,     incrogaps,      {.i = -1 } },
	{ KeyPress,   MODKEY|Mod4Mask|ControlMask,  Key_h,     incrigaps,      {.i = +1 } },
	{ KeyPress,   MODKEY|Mod4Mask|ControlMask,  Key_l,     incrigaps,      {.i = -1 } },
	{ KeyPress,   MODKEY|Mod4Mask,              Key_0,     togglegaps,     {0} },
	{ KeyPress,   MODKEY|Mod4Mask|ShiftMask,    Key_0,     defaultgaps,    {0} },
	{ KeyPress,   MODKEY,                       Key_y,     incrihgaps,     {.i = +1 } },
	{ KeyPress,   MODKEY,                       Key_o,     incrihgaps,     {.i = -1 } },
	{ KeyPress,   MODKEY|ControlMask,           Key_y,     incrivgaps,     {.i = +1 } },
	{ KeyPress,   MODKEY|ControlMask,           Key_o,     incrivgaps,     {.i = -1 } },
	{ KeyPress,   MODKEY|Mod4Mask,              Key_y,     incrohgaps,     {.i = +1 } },
	{ KeyPress,   MODKEY|Mod4Mask,              Key_o,     incrohgaps,     {.i = -1 } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_y,     incrovgaps,     {.i = +1 } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_o,     incrovgaps,     {.i = -1 } },
	{ KeyPress,   MODKEY,                       Key_Return,zoom,           {0} },
	{ KeyPress,   MODKEY,                       Key_Tab,   view,           {0} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_c,     killclient,     {0} },
	{ KeyPress,   MODKEY,                       Key_t,     setlayout,      {.v = &layouts[0]} },
	{ KeyPress,   MODKEY,                       Key_f,     setlayout,      {.v = &layouts[1]} },
	{ KeyPress,   MODKEY,                       Key_m,     setlayout,      {.v = &layouts[2]} },
	{ KeyPress,   MODKEY,                       Key_space, setlayout,      {0} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_space, togglefloating, {0} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_space, togglealwaysontop, {0} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_f,     togglefullscr,  {0} },
	{ KeyPress,   MODKEY,                       Key_0,     view,           {.ui = ~0 } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_0,     tag,            {.ui = ~0 } },
	{ KeyPress,   MODKEY,                       Key_comma, focusmon,       {.i = -1 } },
	{ KeyPress,   MODKEY,                       Key_period,focusmon,       {.i = +1 } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_comma, tagmon,         {.i = -1 } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_period,tagmon,         {.i = +1 } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_b,     spawnorfocus,   {.v = "firefox" } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_bracketright, toggleproc, {.v = "gammastep"} },
	{ KeyPress,   MODKEY|ShiftMask|ControlMask, XK_backslash, toggleisgamemodon, {0} },
	TAGKEYS(                        Key_1,                     0)
	TAGKEYS(                        Key_2,                     1)
	TAGKEYS(                        Key_3,                     2)
	TAGKEYS(                        Key_4,                     3)
	TAGKEYS(                        Key_5,                     4)
	TAGKEYS(                        Key_6,                     5)
	TAGKEYS(                        Key_7,                     6)
	TAGKEYS(                        Key_8,                     7)
	TAGKEYS(                        Key_9,                     8)
	{ KeyPress,   MODKEY|ShiftMask,             Key_q,     quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

