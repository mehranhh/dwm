/* See LICENSE file for copyright and license details. */
/* patches */
#define PATCHATTACHBOTTOM 1
#define PATCHALWAYSCENTER 1
#define BARPERTAG 0

#define SYSTEMD 1

/* appearance */
static const unsigned int borderpx         = 1;  /* border pixel of windows */
static const unsigned int snap             = 32; /* snap pixel */
static const unsigned int gappih           = 9;  /* horiz inner gap between windows */
static const unsigned int gappiv           = 9;  /* vert inner gap between windows */
static const unsigned int gappoh           = 6;  /* horiz outer gap between windows and screen edge */
static const unsigned int gappov           = 6;  /* vert outer gap between windows and screen edge */
static const int smartgaps                 = 1;  /* 1 means no outer gap when there is only one window */
static const unsigned int systraypinning   = 0;  /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft    = 0;	 /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayspacing   = 2;  /* systray spacing */
static const int systraypinningfailfirst   = 1;  /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray               = 1;     /* 0 means no systray */
static const int showbar                   = 0;     /* 0 means no bar */
static const int topbar                    = 1;     /* 0 means bottom bar */
static const char *fonts[]                 = { "monospace:size=9" };
static const char dmenufont[]              = "monospace:size=9";
static const char col_gray1[]              = "#222222";
static const char col_gray2[]              = "#444444";
static const char col_gray3[]              = "#bbbbbb";
static const char col_gray4[]              = "#eeeeee";
static const char col_cyan[]               = "#f94144";
static const char *colors[][3]             = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

static const char *const autostart[] = {
#if !SYSTEMD
	"pipewire", NULL,
	"pipewire-pulse", NULL,
	"pipewire-media-session", NULL,
	"/bin/sh", "-c", "sleep 1 && pipewire -c /home/mhn/.config/noice-suppression-for-voice/filter-chain.conf", NULL,
	"brightnessctl", "-d", "intel_backlight", "set", "50%", NULL,
	"brightnessctl", "-d", "asus::kbd_backlight", "set", "1", NULL,
#else
#endif
	"xautolock", "-time", "10", "-locker", "slock", "-detectsleep", "-notify", "30", "-notifier", "notify-send -u critical -t 10000 'Lock' 'Lock after 30s.'", NULL,
	"lf", "-server", NULL,
	"slstatus", NULL,
	"dunst", "~/.config/dunst/dunstrc", NULL,
	"feh", "--bg-scale", "/usr/share/backgrounds/wallpapers/wallpaper_7.jpg", NULL,
	"picom", NULL,
	"udiskie", NULL,
	"kdeconnect-cli", "--refresh", NULL,
	// "lxqt-policykit-agent", NULL,
	"/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1", NULL,
	"nm-applet", NULL,
	// "solaar", "-w", "hide", NULL,
	// "blueman-applet", NULL,
	"xset", "r", "rate", "300", "30", NULL,
	// "asusctl", "profile", "-P", "Quiet", NULL,
	NULL /* terminate */
};

#define LM 0
#define RM 1
#define SM -1

static const Rule rules[] = {
	/* xprop(1):
	 *  WM_CLASS(STRING) = instance, class
	 *  WM_NAME(STRING) = title
	 */
	/* name             class             instance    title       tags mask     isfloating   monitor    float x,y,w,h         floatborderpx   command*/
	{ "firefox",        "firefox",        NULL,       NULL,       1 << 1,       0,           SM,        50,50,500,500,        1,              {"firefox", NULL} },
	// { "librewolf",      "librewolf",      NULL,       NULL,       1 << 1,       0,           SM,        50,50,500,500,        1,              {"flatpak", "run", "--branch=stable", "--arch=x86_64", "--command=librewolf", "--file-forwarding", "io.gitlab.librewolf-community", NULL} },
	{ "librewolf",      "librewolf",      NULL,       NULL,       1 << 1,       0,           SM,        50,50,500,500,        1,              {"librewolf", NULL} },
	{ "chromium",       "Chromium",       NULL,       NULL,       1 << 1,       0,           SM,        50,50,500,500,        1,              {"chromium", NULL} },
	{ "freetube",       "FreeTube",       NULL,       NULL,       1 << 1,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "mranger",        "st",             "Mranger",  NULL,       1 << 2,       0,           LM,        50,50,500,500,        1,              {"st", "-n", "Mranger", "-c", "st", "-t", "ranger", "-e", "ranger", NULL} },
	{ "mlf",            "st",             "Mlf",      NULL,       1 << 2,       0,           LM,        50,50,500,500,        1,              {"st", "-n", "Mlf", "-c", "st", "-t", "lf", "-e", "lfub", NULL} },
	// { "mpv",            "mpv",            NULL,       NULL,       1 << 2,       0,           -1,        50,50,500,500,        1,              {NULL} },
	{ "nuclear",        "nuclear",        NULL,       NULL,       1 << 2,       0,           RM,        50,50,500,500,        1,              {"flatpak", "run", "org.js.nuclear.Nuclear", NULL} },
	{ "code-oss",       "code-oss",       NULL,       NULL,       1 << 3,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "vscodium",       "VSCodium",       NULL,       NULL,       1 << 3,       0,           LM,        50,50,500,500,        1,              {NULL} },
	{ "mnvim",          "st",             "Mnvim",    NULL,       1 << 3,       0,           LM,        50,50,500,500,        1,              {"st", "-n", "Mnvim", "-c", "st", "-t", "Neovim", "-e", "tmux", "new", "-s", "nvim", "-n", "nvim", "-AD", "nvim", NULL} },
	// { "mnvim",          "Alacritty",      "Mnvim",    NULL,       1 << 3,       0,           LM,        50,50,500,500,        1,              {"alacritty", "--class", "st", "-t", "Neovim", "-e", "tmux", "new", "-s", "nvim", "-n", "nvim", "-AD", "nvim", NULL} },
	// { "mnvim",          "st",             "Mnvim",    NULL,       1 << 3,       0,           LM,        50,50,500,500,        1,              {"st", "-n", "Mnvim", "-c", "st", "-t", "Neovim", "-e", "tmux", "new", "-s", "nvim", "-n", "nvim", "-AD", "nvim", NULL} },
	// { "mnvim",          "st",             "Mnvim",    NULL,       1 << 3,       0,           LM,        50,50,500,500,        1,              {"st", "-n", "Mnvim", "-c", "st", "-t", "Neovim", "-e", "nvim", NULL} },
	{ "neovide",        "neovide",        NULL,       NULL,       1 << 3,       0,           LM,        50,50,500,500,        1,              {"neovide", NULL} },
	{ "neovide-mg",     "neovide",        NULL,       NULL,       1 << 3,       0,           LM,        50,50,500,500,        1,              {"neovide", "--multigrid", NULL} },
	{ "insomnia",       "Insomnia",       NULL,       NULL,       1 << 3,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "virt-manager",   "Virt-manager",   NULL,       NULL,       1 << 4,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "dota2",          "dota2",          NULL,       NULL,       1 << 4,       0,           LM,        50,50,500,500,        1,              {NULL} },
	{ "wowclassic.exe", "wowclassic.exe", NULL,       NULL,       1 << 4,       0,           LM,        50,50,500,500,        1,              {NULL} },
	{ "discord",        "discord",        NULL,       NULL,       1 << 5,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "ferdium",        "Ferdium",        NULL,       NULL,       1 << 5,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "lutris",         "Lutris",         NULL,       NULL,       1 << 6,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "steam",          "Steam",          NULL,       NULL,       1 << 6,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "wowup",          "WowUp",          NULL,       NULL,       1 << 6,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "gimp",           "Gimp",           NULL,       NULL,       1 << 7,       0,           SM,        50,50,500,500,        1,              {NULL} },
	{ "krita",          "krita",          NULL,       NULL,       1 << 7,       0,           SM,        50,50,500,500,        1,              {NULL} },
	{ "obs",            "obs",            NULL,       NULL,       1 << 7,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "qbittorrent",    "qBittorrent",    NULL,       NULL,       1 << 8,       0,           RM,        50,50,500,500,        1,              {NULL} },
	{ "mcmus",          "st",             "Mcmus",    NULL,       0,            1,           SM,        55,40,1820,991,       1,              {"st", "-n", "Mcmus", "-c", "st", "-t", "cmus", "-e", "cmus", NULL} },
	{ "mbtop",          "st",             "Mbtop",    NULL,       0,            1,           SM,        55,40,1820,991,       1,              {"st", "-n", "Mbtop", "-c", "st", "-t", "btop", "-e", "btop", "-p", "0", NULL} },
	{ "pavucontrol",    "Pavucontrol",    NULL,       NULL,       0,            1,           SM,        1413,473,500,600,     1,              {"pavucontrol", "--tab=4", NULL} },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

static const TagData tags[] = { 
	// symbol,  layout,            mfact,    nmasters, showbars
	{ "",      &layouts[0],       mfact,    nmaster,  showbar },
	{ "",      &layouts[2],       mfact,    nmaster,  0 },
	{ "",      &layouts[0],       mfact,    nmaster,  0 },
	{ "",      &layouts[2],       mfact,    nmaster,  0 },
	{ "",      &layouts[0],       mfact,    nmaster,  showbar },
	{ "",      &layouts[2],       mfact,    nmaster,  showbar },
	{ "",      &layouts[0],       0.75f,    nmaster,  showbar },
	{ "",      &layouts[0],       mfact,    nmaster,  showbar },
	{ "",      &layouts[0],       mfact,    nmaster,  showbar },
};

static ToggleProc toggleprocs[] = {
	/* id               autosatrt   signal    notification ID  notification title     notification icon          cmd           not my real location                      0 */
	{ "gammastep",      1,          SIGTERM,  "8001",          "Blue Light Filter",   "eye-solid",               {"gammastep", "-l", "36.51212:51.1251775", "-r", NULL}, 0},
	{ "autoclicker",    0,          SIGTERM,  "8002",          "Auto Clicker",        "computer-mouse-solid",    {"autoclicker", NULL},                                  0},
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ KeyPress,   MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ KeyPress,   MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} },
	// { KeyPress,   MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]  = { "alacritty", NULL };

/* this must be your default keyboard config */
static const char *cmdgamemode0keyboard[] = {"setxkbmap", "-layout", "us,ir,us", "-model", "pc104", "-variant", "colemak,,", "-option", "caps:backspace,grp:alt_shift_toggle", NULL };
static const char *cmdgamemode1keyboard[] = {"setxkbmap", "-layout", "us", "-model", "pc104", "-variant", "colemak", NULL };

#include "movestack.c"
#include "keys.h"
static Key keys[] = {
	/* type       modifier                     key                     function                  argument */
	{ KeyPress,   MODKEY,                       Key_q,                  spawn,                    {.v = termcmd} },
	/* window management */
	{ KeyPress,   MODKEY|ShiftMask,             Key_f,                  togglefloating,           {0} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_a,                  togglealwaysontop,        {0} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_e,                  togglefullscr,            {0} },
	{ KeyPress,   MODKEY,                       Key_grave,              togglebar,                {0} },
	{ KeyPress,   MODKEY|ShiftMask|ControlMask, Key_backslash,          toggleisgamemodon,        {0} },
	{ KeyPress,   MODKEY,                       Key_a,                  focusstack,               {.i = +1} },
	{ KeyPress,   MODKEY,                       Key_s,                  focusstack,               {.i = -1} },
	{ KeyPress,   MODKEY,                       Key_f,                  incnmaster,               {.i = -1} },
	{ KeyPress,   MODKEY,                       Key_d,                  incnmaster,               {.i = +1} },
	{ KeyPress,   MODKEY,                       Key_h,                  setmfact,                 {.f = -0.05} },
	{ KeyPress,   MODKEY,                       Key_l,                  setmfact,                 {.f = +0.05} },
	{ KeyPress,   MODKEY,                       Key_j,                  focusstack,               {.i = +1} },
	{ KeyPress,   MODKEY,                       Key_k,                  focusstack,               {.i = -1} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_j,                  movestack,                {.i = +1 } },
	{ KeyPress,   MODKEY|ShiftMask,             Key_k,                  movestack,                {.i = -1 } },
	{ KeyPress,   MODKEY,                       Key_Return,             zoom,                     {0} },
	{ KeyPress,   MODKEY,                       Key_Tab,                view,                     {0} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_c,                  killclient,               {0} },
	{ KeyPress,   MODKEY,                       Key_t,                  setlayout,                {.v = &layouts[0]}},
	{ KeyPress,   MODKEY|ShiftMask,             Key_r,                  setlayout,                {.v = &layouts[1]}},
	{ KeyPress,   MODKEY,                       Key_r,                  setlayout,                {.v = &layouts[2]}},
	// { KeyPress,   MODKEY,                       Key_0,                  view,                     {.ui = ~0} },
	// { KeyPress,   MODKEY|ShiftMask,             Key_0,                  tag,                      {.ui = ~0} },
	{ KeyPress,   MODKEY,                       Key_comma,              focusmon,                 {.i = -1} },
	{ KeyPress,   MODKEY,                       Key_period,             focusmon,                 {.i = +1} },
	{ KeyPress,   MODKEY,                       Key_w,                  focusmon,                 {.i = +1} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_comma,              tagmon,                   {.i = -1} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_period,             tagmon,                   {.i = +1} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_w,                  tagmon,                   {.i = +1} },
	{ KeyPress,   MODKEY|ShiftMask|ControlMask, Key_q,                  quit,                     {0} },
	TAGKEYS(                        Key_1,                                                0)
	TAGKEYS(                        Key_2,                                                1)
	TAGKEYS(                        Key_3,                                                2)
	TAGKEYS(                        Key_4,                                                3)
	TAGKEYS(                        Key_5,                                                4)
	TAGKEYS(                        Key_6,                                                5)
	TAGKEYS(                        Key_7,                                                6)
	TAGKEYS(                        Key_8,                                                7)
	TAGKEYS(                        Key_9,                                                8)
	/* window rules */
	{ KeyPress,   MODKEY,                       Key_b,                  spawnorfocus,             {.v = "librewolf"} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_b,                  spawnorfocus,             {.v = "chromium"} },
	{ KeyPress,   MODKEY,                       Key_e,                  spawnorfocus,             {.v = "mlf"} },
	{ KeyPress,   MODKEY,                       Key_c,                  spawnorfocus,             {.v = "mnvim"} },
	{ KeyPress,   MODKEY,                       Key_v,                  spawnorfocus,             {.v = "mcmus"} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_v,                  spawnorfocus,             {.v = "mpv"} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_m,                  spawnorfocus,             {.v = "nuclear"} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_p,                  spawnorfocus,             {.v = "mbtop"} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_o,                  spawnorfocus,             {.v = "pavucontrol"} },
	/* toggle procs */
	{ KeyPress,   MODKEY|ShiftMask,             Key_bracketright,       toggleproc,               {.v = "gammastep"} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_grave,              toggleproc,               {.v = "autoclicker"} },
	/* general tasks */
	{ KeyPress,   MODKEY|Mod1Mask,              Key_space,              spawngamemodeoff,         {.v = (char *[]){"mcontrol", "menu_path", NULL}} },
	{ KeyPress,   Mod1Mask,                     Key_space,              spawngamemodeoff,         {.v = (char *[]){"mcontrol", "menu_desktop", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_q,                  spawn,                    {.v = (char *[]){"mcontrol", "menu_shutdown", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_l,                  spawn,                    {.v = (char *[]){"mcontrol", "xlock", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_backslash,          spawn,                    {.v = (char *[]){"mcontrol", "xset_rate", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_equal,              spawn,                    {.v = (char *[]){"mcontrol", "xlock_enable", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_minus,              spawn,                    {.v = (char *[]){"mcontrol", "xlock_disable", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_slash,              spawn,                    {.v = (char *[]){"mcontrol", "do_not_disturbe", NULL}} },
	{ KeyPress,   Mod5Mask,                     Key_backslash,          spawn,                    {.v = (char *[]){"mcontrol", "kdeconnect_refresh", NULL}} },
	{ KeyPress,   Mod5Mask|ShiftMask,           Key_backslash,          spawn,                    {.v = (char *[]){"mcontrol", "kdeconnect_kill", NULL}} },
	{ KeyPress,   0,                            Key_XF86AudioLowerVolume,spawn,                   {.v = (char *[]){"mcontrol", "pa_vol_add", "-0.05", "1.0", NULL}} },
	{ KeyPress,   0,                            Key_XF86AudioRaiseVolume,spawn,                   {.v = (char *[]){"mcontrol", "pa_vol_add", "+0.05", "1.0", NULL}} },
	{ KeyPress,   0,                            Key_XF86AudioMute,      spawn,                    {.v = (char *[]){"mcontrol", "pa_vol_toggle", NULL}} },
	{ KeyPress,   MODKEY,                       Key_XF86AudioLowerVolume,spawn,                   {.v = (char *[]){"mcontrol", "pa_mic_add", "-0.05", "1.0", NULL}} },
	{ KeyPress,   MODKEY,                       Key_XF86AudioRaiseVolume,spawn,                   {.v = (char *[]){"mcontrol", "pa_mic_add", "+0.05", "1.0", NULL}} },
	{ KeyPress,   MODKEY,                       Key_XF86AudioMute,      spawn,                    {.v = (char *[]){"mcontrol", "pa_mic_toggle", NULL}} },
	{ KeyPress,   MODKEY,                       Key_F1,                 spawn,                    {.v = (char *[]){"mcontrol", "pa_mic_toggle", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask,             Key_s,                  spawn,                    {.v = (char *[]){"mcontrol", "xscreenshot_clipboard_selection", NULL}} },
	{ KeyPress,   MODKEY|ShiftMask|ControlMask, Key_s,                  spawn,                    {.v = (char *[]){"mcontrol", "xscreenshot_file_selection", NULL}} },
	{ KeyPress,   0,                            Key_Print,              spawn,                    {.v = (char *[]){"mcontrol", "xscreenshot_clipboard", NULL}} },
	{ KeyPress,   ShiftMask,                    Key_Print,              spawn,                    {.v = (char *[]){"mcontrol", "xscreenshot_file", NULL}} },
	// { KeyPress,   0,                            Key_XF86MonBrightnessDown,spawn,                  {.v = (char *[]){"mcontrol", "mon_brightness_down", NULL}} },
	// { KeyPress,   0,                            Key_XF86MonBrightnessUp,spawn,                    {.v = (char *[]){"mcontrol", "mon_brightness_up", NULL}} },
	// { KeyPress,   Mod5Mask,                     Key_F7,                 spawn,                    {.v = (char *[]){"mcontrol", "mon_brightness_down", NULL}} },
	// { KeyPress,   Mod5Mask,                     Key_F8,                 spawn,                    {.v = (char *[]){"mcontrol", "mon_brightness_up", NULL}} },
	// { KeyPress,   0,                            Key_XF86KbdBrightnessDown,spawn,                  {.v = (char *[]){"mcontrol", "keyboard_brightness_down", NULL}} },
	// { KeyPress,   0,                            Key_XF86KbdBrightnessUp,spawn,                    {.v = (char *[]){"mcontrol", "keyboard_brightness_up", NULL}} },
	{ KeyPress,   0,                            Key_XF86AudioPrev,      spawn,                    {.v = (char *[]){"mcontrol", "player_prev", NULL}} },
	{ KeyPress,   0,                            Key_XF86AudioPlay,      spawn,                    {.v = (char *[]){"mcontrol", "player_play_pause", NULL}} },
	{ KeyPress,   0,                            Key_XF86AudioNext,      spawn,                    {.v = (char *[]){"mcontrol", "player_next", NULL}} },
	{ KeyPress,   ShiftMask,                    Key_XF86AudioPrev,      spawn,                    {.v = (char *[]){"mcontrol", "player_prev_selective", NULL}} },
	{ KeyPress,   ShiftMask,                    Key_XF86AudioPlay,      spawn,                    {.v = (char *[]){"mcontrol", "player_play_pause_selective", NULL}} },
	{ KeyPress,   ShiftMask,                    Key_XF86AudioNext,      spawn,                    {.v = (char *[]){"mcontrol", "player_next_selective", NULL}} },
	// { KeyPress,   0,                            Key_XF86Launch2,        spawn,                    {.v = (char *[]){"mcontrol", "asusctl_led_mode_prev", NULL}} },
	// { KeyPress,   0,                            Key_XF86Launch3,        spawn,                    {.v = (char *[]){"mcontrol", "asusctl_led_mode_next", NULL}} },
	// { KeyPress,   0,                            Key_XF86Launch4,        spawn,                    {.v = (char *[]){"mcontrol", "asusctl_profile_next", NULL}} },
	// { KeyPress,   Mod5Mask,                     Key_F5,                 spawn,                    {.v = (char *[]){"mcontrol", "asusctl_profile_next", NULL}} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

